Slides package
==============

Submodules
----------

Slides.cell\_manager module
---------------------------

.. automodule:: Slides.cell_manager
    :members:
    :undoc-members:
    :show-inheritance:

Slides.count\_slides module
---------------------------

.. automodule:: Slides.count_slides
    :members:
    :undoc-members:
    :show-inheritance:

Slides.inject\-markdown module
------------------------------

.. automodule:: Slides.inject-markdown
    :members:
    :undoc-members:
    :show-inheritance:

Slides.math\_helper module
--------------------------

.. automodule:: Slides.math_helper
    :members:
    :undoc-members:
    :show-inheritance:

Slides.presentation\_helper module
----------------------------------

.. automodule:: Slides.presentation_helper
    :members:
    :undoc-members:
    :show-inheritance:

Slides.snippet\_helper module
-----------------------------

.. automodule:: Slides.snippet_helper
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: Slides
    :members:
    :undoc-members:
    :show-inheritance:
