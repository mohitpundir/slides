#!/bin/env python3

################################################################
import nbformat
import sys
################################################################


def countSlides(notebook_files):

    notebooks = [nbformat.read(f, as_version=4)
                 for f in notebook_files]

    cpt = 0

    for notebook in notebooks:
        cells = notebook['cells']
        for c in cells:
            if 'slideshow' not in c.metadata:
                continue
            md = c.metadata.slideshow
            if 'slide_type' in md:
                if md.slide_type == 'slide':
                    cpt += 1
                if md.slide_type == 'subslide':
                    cpt += 1

    return cpt

################################################################


if __name__ == "__main__":
    notebook_files = sys.argv[1:]
    nb_slides = countSlides(notebook_files)
    print("found {0} slides".format(nb_slides))

################################################################
