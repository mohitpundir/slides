"""LaTeX Exporter class"""

# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

import os

from traitlets import Unicode, default
from traitlets.config import Config
from copy import deepcopy
from nbconvert.filters.highlight import Highlight2Latex
from nbconvert.filters.filter_links import resolve_references
from nbconvert.exporters.templateexporter import TemplateExporter


def prepare(nb):
    """Add some convenience metadata on cells for the slide template."""
    nb = deepcopy(nb)

    for cell in nb.cells:
        # Make sure every cell has a slide_type
        cell.metadata.slide_type = cell.metadata.get(
            'slideshow', {}).get('slide_type', '-')

    # Find the first visible cell
    for index, cell in enumerate(nb.cells):
        if cell.metadata.slide_type not in {'notes', 'skip'}:
            cell.metadata.slide_type = 'slide'
            cell.metadata.slide_start = True
            cell.metadata.subslide_start = True
            first_slide_ix = index
            break
    else:
        raise ValueError("All cells are hidden, cannot create slideshow")

    in_fragment = False

    for index, cell in enumerate(nb.cells[first_slide_ix+1:],
                                 start=(first_slide_ix+1)):

        previous_cell = nb.cells[index - 1]

        # Get the slide type. If type is subslide or slide,
        # end the last slide/subslide/fragment as applicable.
        if cell.metadata.slide_type == 'slide':
            previous_cell.metadata.slide_end = True
            cell.metadata.slide_start = True
        if cell.metadata.slide_type in {'subslide', 'slide'}:
            previous_cell.metadata.fragment_end = in_fragment
            previous_cell.metadata.subslide_end = True
            cell.metadata.subslide_start = True
            in_fragment = False

        elif cell.metadata.slide_type == 'fragment':
            cell.metadata.fragment_start = True
            if in_fragment:
                previous_cell.metadata.fragment_end = True
            else:
                in_fragment = True

        if cell.metadata.get('cell_style') == 'split':
            cell.metadata.split = True

    # The last cell will always be the end of a slide
    nb.cells[-1].metadata.fragment_end = in_fragment
    nb.cells[-1].metadata.subslide_end = True
    nb.cells[-1].metadata.slide_end = True

    return nb


class BeamerExporter(TemplateExporter):
    """
    Exports to a Latex template.  Inherit from this class
    if your template is LaTeX based and you need custom
    tranformers/filters.
    Inherit from it if you are writing your own HTML template
    and need custom tranformers/filters.
    If you don't need custom tranformers/filters, just change the
    'template_file' config option.  Place your template in the
    special "/latex" subfolder of the "../templates" folder.
    """

    @default('file_extension')
    def _file_extension_default(self):
        return '.tex'

    @default('template_file')
    def _template_file_default(self):
        return 'article'

    # Latex constants
    @default('default_template_path')
    def _default_template_path_default(self):
        return os.path.join("..", "templates", "latex")

    @default('template_skeleton_path')
    def _template_skeleton_path_default(self):
        return os.path.join("..", "templates", "latex", "skeleton")

    # Extension that the template files use.
    template_extension = Unicode(".tplx").tag(config=True)

    output_mimetype = 'text/latex'

    def default_filters(self):
        for x in super(BeamerExporter, self).default_filters():
            yield x
        yield ('resolve_references', resolve_references)

    @property
    def default_config(self):
        c = Config({
            'NbConvertBase': {
                'display_data_priority': ['text/latex',
                                          'application/pdf',
                                          'image/png',
                                          'image/jpeg',
                                          'image/svg+xml',
                                          'text/markdown',
                                          'text/plain']
                },
            'ExtractOutputPreprocessor': {
                'enabled': True
            },
            'SVG2PDFPreprocessor': {
                'enabled': True
            },
            'LatexPreprocessor': {
                'enabled': True
            },
            'SphinxPreprocessor': {
                'enabled': True
            },
            'HighlightMagicsPreprocessor': {
                'enabled': True
            }
         })
        c.merge(super(BeamerExporter, self).default_config)
        return c

    def from_notebook_node(self, nb, resources=None, **kw):
        langinfo = nb.metadata.get('language_info', {})
        lexer = langinfo.get('pygments_lexer', langinfo.get('name',
                                                            None))
        self.register_filter('highlight_code',
                             Highlight2Latex(pygments_lexer=lexer,
                                             parent=self))
        return super(BeamerExporter, self).from_notebook_node(nb,
                                                              resources,
                                                              **kw)

    def _create_environment(self):
        environment = super(BeamerExporter, self)._create_environment()

        # Set special Jinja2 syntax that will not conflict with latex.
        environment.block_start_string = "((*"
        environment.block_end_string = "*))"
        environment.variable_start_string = "((("
        environment.variable_end_string = ")))"
        environment.comment_start_string = "((="
        environment.comment_end_string = "=))"

        return environment
