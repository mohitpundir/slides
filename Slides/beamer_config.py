import os
import sys


dirname = os.path.dirname(__file__)
print(dirname)
sys.path.append(dirname)

c = get_config()
c.Exporter.template_file = 'beamer.tplx'
print(c.Exporter.__dict__)
c.Exporter.template_paths.append(os.path.join(dirname, 'templates'))
c.Exporter.template_paths.append('/home/pundir/.local/share/virtualenvs/slides-rLR_5cAf/share/jupyter/nbconvert/templates/latex')
print(c.Exporter.items())
c.Exporter.preprocessors = [
    'jupyter_contrib_nbextensions.nbconvert_support.pre_pymarkdown.'
    'PyMarkdownPreprocessor']
