#!/bin/env python3

import os
import subprocess
import shutil
import nbformat
################################################################


class NotFoundNotebooks(Exception):
    "Could not find the notebooks to treat"

################################################################


def mergeNotebooks(notebook_files, add_cell_number=False,
                   select_functor=None, modify_functor=None,
                   out_filename='talk'):
    "Merges several notebooks in a single one"

    def _select_functor(_i, _cell):
        return True

    if select_functor is None:
        select_functor = _select_functor

    def _modify_functor(_i, _cell):
        return _cell

    if modify_functor is None:
        modify_functor = _modify_functor

    print("Merging notebooks {0}".format(notebook_files))
    notebooks = [nbformat.read(f, as_version=4) for f in notebook_files]

    if len(notebooks) == 0:
        raise NotFoundNotebooks

    cells = notebooks[0]['cells']
    for n in notebooks[1:]:
        cells += n['cells']

    new_cells = []
    for cell_number, c in enumerate(cells):
        if not select_functor(cell_number, c):
            continue
        # print(c)
        new_cells.append(modify_functor(cell_number, c))
        if add_cell_number:
            new_cell = nbformat.notebooknode.NotebookNode()
            new_cell['cell_type'] = 'markdown'
            new_cell['source'] = 'Cell number {0}'.format(cell_number)
            new_cell['metadata'] = {}
            new_cells.append(new_cell)
        # print(new_cells)
    notebooks[0]['cells'] = new_cells

    nbformat.write(notebooks[0], out_filename)

################################################################


def launchSlideServer(filename, options="", font_size="25px"):
    " Launch the HTTP server for reveal slides "
    dir_name = os.path.dirname(__file__)
    slides_config_file = os.path.join(dir_name, 'slides_config.py')
    #reveal_path = os.path.join(dir_name, '..', 'js', 'reveal.js')
    #print(reveal_path)
    # cmd = ('jupyter nbconvert --to=Slides.myslides.SlidesExporter
    #         --config {1} --post serve '
    #        '--ServePostProcessor.open_in_browser=False'
    #        ' --reveal-prefix={0} {2} talk.ipynb').format(
    #                    reveal_path, slides_config_file, options)
    cmd = ('jupyter nbconvert --to=Slides.myslides.SlidesExporter '
           f'--config {slides_config_file} ' 
           f'--post serve --SlidesExporter.font_size={font_size} '
           f'--ServePostProcessor.open_in_browser=False {filename}')
    print(cmd)
    subprocess.call(cmd, shell=True)

################################################################


def generateHTML(filename, options="", font_size="25px"):
    " Launch the HTTP server for reveal slides "
    dir_name = os.path.dirname(__file__)
    slides_config_file = os.path.join(dir_name, 'slides_config.py')
    #reveal_path = os.path.join(dir_name, 'js', './reveal.js')
    # cmd = ('jupyter nbconvert --to=Slides.myslides.SlidesExporter
    #          --config {1}'
    #        ' --reveal-prefix={0} {2} talk.ipynb').format(
    #                    reveal_path, slides_config_file, options)

    cmd = ('jupyter nbconvert --to=Slides.myslides.SlidesExporter '
           f'--config {slides_config_file} --SlidesExporter.pdf_format="true" '
           f'--SlidesExporter.font_size={font_size}'
           f' {options} {filename}')

    print(cmd)
    subprocess.call(cmd, shell=True)


################################################################


def generateBeamer(filename, options="", font_size="25px"):
    " Launch the HTTP server for reveal slides "
    dir_name = os.path.dirname(__file__)
    slides_config_file = os.path.join(dir_name, 'beamer_config.py')
    # reveal_path = os.path.join(dir_name, 'js', './reveal.js')
    cmd = ('jupyter nbconvert --to=Slides.beamer.BeamerExporter '
           f'--config {slides_config_file} '
           f'--BeamerExporter.font_size={font_size}'
           f' {options} {filename}')

    print(cmd)
    subprocess.call(cmd, shell=True)


################################################################


def executeNotebook(filename):
    " Request the execution of all cells "
    print('execute notebook: ', filename)
    dir_name = os.path.dirname(__file__)
    slides_config_file = os.path.join(dir_name, 'slides_execute_config.py')
    subprocess.call('jupyter nbconvert --NotebookClient.timeout=-1 --ExecutePreprocessor.timeout=-1 --to=notebook --output tmp.ipynb'
                    ' --config {0} {1}'.format(
                        slides_config_file, filename), shell=True)
    shutil.move('tmp.ipynb', filename)
    subprocess.call('jupyter trust {0}'.format(filename), shell=True)

################################################################
