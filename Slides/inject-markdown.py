#!/usr/bin/python3

import subprocess
import sys

# import nbformat
# import io
# import re
# import os
# from Queue import Empty  # Py 2

notebook_files = sys.argv[1:]

for f in notebook_files:
    cmd = (
       'jupyter nbconvert --inplace --to=notebook '
       '--config markdown_config.py {0}'.format(f))
    print(cmd)
    subprocess.call(cmd, shell=True)
