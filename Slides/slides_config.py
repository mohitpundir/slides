#! /usr/bin/env python3

import os
import sys


dirname = os.path.dirname(__file__)
# print(dirname)
sys.path.append(dirname)


c = get_config()
c.Exporter.template_file = 'presentation.tpl'
#c.Exporter.template_file = 'mybasic.tpl'
c.Exporter.template_paths.append(os.path.join(dirname, 'templates'))
c.Exporter.template_paths.append('/home/pundir/.local/share/virtualenvs/slides-rLR_5cAf/share/jupyter/nbconvert/templates/')
print(c.Exporter.items())
# c.Exporter.preprocessors = ['execute_markdown.ExecutePreprocessor',
# 'pre_pymarkdown.PyMarkdownPreprocessor']
c.Exporter.preprocessors = [
    'jupyter_contrib_nbextensions.nbconvert_support.pre_pymarkdown.'
    'PyMarkdownPreprocessor']
