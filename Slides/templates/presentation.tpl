{%- extends 'mybasic.tpl' -%}
{# ######################################## #}
{# definition of the treatment of the cells #}
{# ######################################## #}

{%- block any_cell scoped -%}
{%- if cell.metadata.get('slide_start', False) -%}
<section>
{%- endif -%}
{%- if cell.metadata.get('subslide_start', False) -%}
<section>
<div class="slide" style="float: left">
{%- endif -%}
{%- if cell.metadata.get('fragment_start', False) -%}
<div class="fragment" style="width: 100%;float: left">
{%- endif -%}

{%- if cell.metadata.slide_type == 'notes' -%}
{%- if cell.metadata.split == False -%}
<aside class="notes" style="width: 100%;float: left">
{%- else -%}
<aside class="notes" style="width: 45%;padding: 2.5%; float: left">
{%- endif -%}
{{ super() }}
</aside>
{%- elif cell.metadata.slide_type == 'skip' -%}
{%- else -%}
{%- if cell.metadata.split == True -%}
<div class="split" style="width: 45%;padding: 2.5%; float: left">
{{ super() }}
</div>
{%- else -%}
<div class="full" style="width: 100%; float: left">
{{ super() }}
</div>
{%- endif -%}
{%- endif -%}

{%- if cell.metadata.get('fragment_end', False) -%}
</div>
{%- endif -%}
{%- if cell.metadata.get('subslide_end', False) -%}
</div>
</section>
{%- endif -%}
{%- if cell.metadata.get('slide_end', False) -%}
</section>
{%- endif -%}

{%- endblock any_cell -%}

{# ######################################## #}
{# header cell                              #}
{# ######################################## #}
{% from 'reveal.tpl' import init_reveal %}
{% from 'reveal.tpl' import load_reveal %}

{% block header %}
<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="chrome=1" />

<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

<title>{{resources['metadata']['name']}} slides</title>

<!-- Loading the init_reveal macro -->
{{ init_reveal(resources.reveal.url_prefix) }}

<style type="text/css">
  {{resources.css}}
</style>
{# <link rel="stylesheet" href="{{resources.css}}">#}
{# {% for css in resources.inlining.css -%}
    <style type="text/css">
    {{ css }}
    </style>
{% endfor %}
#}


</head>
{% endblock header%}

{# ######################################## #}
{# body cell                                #}
{# ######################################## #}

{% block body %}
<body>

<div class="reveal">
<div class="slides">
{{ super() }}
</div>
</div>

<!-- loads reveal -->
{{ load_reveal(resources.reveal.url_prefix) }}

</body>
{% endblock body %}

{% block footer %}
</html>
{% endblock footer %}