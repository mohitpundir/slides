<style type="text/css">
/* Overrides of notebook CSS for static HTML export */
.reveal {
  font-size: 160%;
  overflow-y: scroll;
}
.reveal pre {
  width: inherit;
  padding: 0.4em;
  margin: 0px;
  font-family: monospace, sans-serif;
  font-size: 80%;
  box-shadow: 0px 0px 0px rgba(0, 0, 0, 0);
}
.reveal pre code {
  padding: 0px;
  overflow: visible;
  overflow: visible;
}
.reveal img {
  border: 0px solid black;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0);
}

section {
  //border: 5px solid black;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0);
}
.reveal i {
  font-style: normal;
  font-family: FontAwesome;
  font-size: 2em;
}
.reveal .slides {
  text-align: left;
  //border: solid black;
}
.reveal.fade {
  opacity: 1;
}
.reveal .progress {
  position: static;
}
div.code_cell {
  background-color: transparent;
}
div.prompt {
  width: 11ex;
  padding: 0.4em;
  margin: 0px;
  font-family: monospace, sans-serif;
  font-size: 80%;
  text-align: right;
}
div.output_area pre {
  font-family: monospace, sans-serif;
  font-size: 80%;
}
div.output_prompt {
  /* 5px right shift to account for margin in parent container */
  margin: 5px 5px 0 0;
}
div.text_cell.rendered .rendered_html {
  /* The H1 height seems miscalculated, we are just hidding the scrollbar */
  overflow-y: hidden;
}
a.anchor-link {
  /* There is still an anchor, we are only hidding it */
  display: none;
}
.rendered_html p {
  text-align: inherit;
}

/* added by guillaume */

div.box {
  border-radius: 12px;
  background-color: black;
  color: white;
  padding: 15px 15px 15px 15px;
  margin: 5px 5px 5px 5px;
  box-shadow: 10px 10px 5px grey;
}

body {
    font-size: 15px;
}

/*hr {
height: 5;
}*/

.output_wrapper {
    //border: 5px solid black
    text-align: center
}
.reveal {
    overflow-y: hidden
}
</style>
