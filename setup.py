from setuptools import setup, find_packages
try:
    from sphinx.setup_command import BuildDoc
    cmdclass = {'doc': BuildDoc}
except Exception:
    cmdclass = {}

name = 'slides'
version = '1.0'
release = '1.0.0'

setup(name=name,
      packages=find_packages(),
      version="0.0.1",
      author="Guillaume Anciaux",
      author_email="guillaume.anciaux@epfl.ch",
      description=("Slides making/convertion tools"),
      doc_require=["sphinx"],
      install_requires=["jupyter", "sympy", "pypdf2",
                        "matplotlib", "wand", "dill", "nbformat",
                        "jupyter_contrib_nbextensions", "pyparsing>=2.3.0"],
      tests_requires=["pytest"],
      setup_requires=["pytest-runner"],
      test_suite="tests",
      cmdclass=cmdclass,
      package_data={
          'Slides': [
              'templates/beamer.tplx',
              'templates/display_priority.tpl',
              'templates/html.css',
              'templates/mathjax.tpl',
              'templates/mybasic.tpl',
              'templates/presentation.tpl',
              'templates/reveal.css',
              'templates/reveal_css.tpl',
              'templates/reveal.tpl'
          ]},
      command_options={
          'doc': {
              'project': ('setup.py', name),
              'version': ('setup.py', version),
              'release': ('setup.py', release),
              'source_dir': ('setup.py', 'doc')}},
      scripts=['scripts/slides',
               'scripts/pLatex'],
      license="""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
""")
