#include <iostream>

int main(int argc, char *argv[]) {
  /* This is a comment and will be ignored by the compiler
Comments are useful to explain in English what
the program does */

  // Print "Hello World" to the screen
  std::cout << "Hello World\\n";
  return 0;

  int row, column;
  double temperature;
  row = 1;
  column = 2;
  temperature = 3.0;

  double tolerance1 = 0.0001;
  double tolerance2 = 1e-4;

  const double density = 45.621;

  int integer1;
  short int integer2;
  long int integer3;

  signed long int integer4;
  unsigned int integer5;

  float x1;
  double x2;
  long double x3;
}
