#!/bin/env python3
# -*- coding: utf-8 -*-

from PythonLatex.latex_structure import LatexStructure


def test_latex_parser_beamer():
    latex_code = r"""
\documentclass[9pt,xcolor=dvipsnames]{beamer}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{fancyvrb}

\begin{python}{header}
from Slides.snippet_helper import Snippet, SnippetCollection
Snippet.default_output='latex'
# Snippet.default_line_numbering=True
\end{python}
\py{Snippet.getLatexStyleDefs()}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{class.tex}
\title{Chapter 4. Pointers}
\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\maketitle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]{Pointers and the Computer's Memory}

\only<1>{
\py{yop}
}
\only<2>{

}

$p_x$

ROW\_MAJOR
\end{frame}
\end{document}
"""

    tex_struct = LatexStructure()
    tex_struct.parseLatex(latex_code)
    output = str(tex_struct)
    f = open('tmp.tex', 'w')
    f.write(latex_code)
    f.close()
    f = open('tmp_output.tex', 'w')
    f.write(output)
    f.close()

    if latex_code != output:
        print(output)

    assert latex_code == output


def test_latex_parser_underscore():
    latex_code = r'ROW\_MAJOR'
    tex_struct = LatexStructure()
    tex_struct.parseLatex(latex_code)
    output = str(tex_struct)
    assert latex_code == output


def test_latex_parser_nested_begin_end():
    latex_code = r'''
\begin{toto}tatie
\begin{re}tututoto \end{re} aa\end{toto}
'''
    tex_struct = LatexStructure()
    tex_struct.parseLatex(latex_code)
    output = str(tex_struct)
    assert latex_code == output


def test_latex_parser_begin_end():
    latex_code = r'\begin{toto}tatie\end{toto}'
    tex_struct = LatexStructure()
    tex_struct.parseLatex(latex_code)
    output = str(tex_struct)
    assert latex_code == output


def test_latex_parser_text():
    latex_code = r'tatie'
    tex_struct = LatexStructure()
    tex_struct.parseLatex(latex_code)
    output = str(tex_struct)
    assert latex_code == output


def test_latex_parser_comment():
    latex_code = r'''
%  tatie
toto
'''
    tex_struct = LatexStructure()
    tex_struct.parseLatex(latex_code)
    output = str(tex_struct)
    assert latex_code == output


def test_latex_parser_command():
    latex_code = r' \documentclass[10pt]{article}'
    tex_struct = LatexStructure()
    tex_struct.parseLatex(latex_code)
    output = str(tex_struct)
    assert latex_code == output


def test_latex_parser_nested_begin_command():
    latex_code = r'''{ yala {toto } \begin{align} tutu
\begin{equation} toto \end{equation}
\end{align} {\titi{ tutu} } }'''
    tex_struct = LatexStructure()
    tex_struct.parseLatex(latex_code)
    output = str(tex_struct)
    assert latex_code == output


def test_latex_env():
    latex_code = r'''
\begin{toto} tata
\end{toto}
\begin{tata}
toto
\end{tata}

\begin{tutu}
titi
\begin{titi}
\end{titi}
\end{tutu}
'''
    tex_struct = LatexStructure()
    tex_struct.parseLatex(latex_code)
    output = str(tex_struct)
    assert latex_code == output

    blk_list = []

    def foo(i):
        blk_list.append(i.name)

    blk_list_correct = ['main',
                        'toto', 'toto',
                        'tata', 'tata',
                        'tutu', 'titi', 'titi', 'tutu',
                        'main']

    tex_struct.pathInBlock(begin_functor=foo, end_functor=foo)
    assert blk_list == blk_list_correct


def test_latex_python_code():
    latex_code = r'''
\begin{python}{cpp}
snippet = Snippet('code_snippets/hello.cpp')
\end{python}
'''
    tex_struct = LatexStructure()
    tex_struct.parseLatex(latex_code)
    output = str(tex_struct)
    assert latex_code == output


def test_latex_frame_includegraphics_only():
    latex_code = r'''
\begin{frame}{What is a computer ?}
\includegraphics<2>[width=\textwidth]{figures/computer-components.png}
\end{frame}

\begin{frame}{What is a program ?}

toto
\end{frame}
'''
    tex_struct = LatexStructure()
    tex_struct.parseLatex(latex_code)
    output = str(tex_struct)
    assert latex_code == output

    blk_list = []

    def foo(i):
        blk_list.append(i.name)

    tex_struct.pathInBlock(begin_functor=foo, end_functor=foo)

    blk_list_correct = ['main',
                        'frame', 'frame',
                        'frame', 'frame',
                        'main']

    assert blk_list == blk_list_correct


def test_latex_item_beamer():
    latex_code = r'''
\begin{document}
\begin{frame}
  \section{Class organization}
  \begin{itemize}
  \item<1-> Teaching staff: G. Anciaux, A. Nielsen, L. Pegolotti.
  \item<2-> Lectures: on Mondays, exercises on Fridays
  \item<3-> Follow chapters of the book: \href{http://link.springer.com/book/10.1007/978-1-4471-2736-9}{Guide To Scientific Computing in C++}
  \item<4-> Permanent homework: reading next chapter of the book
  \item<5-> Moodle (password: PCSC2017): material, forum (at the beginning
  \item<6-> Git: material, pdfs, solutions
  \item<7-> Evaluation: project realization and oral presentation
  \end{itemize}
\end{frame}

\begin{frame}

\end{frame}
\end{document}
'''
    tex_struct = LatexStructure()
    tex_struct.parseLatex(latex_code)
    output = str(tex_struct)
    assert latex_code == output

    blk_list = []

    def foo(i):
        blk_list.append(i.name)

    tex_struct.pathInBlock(begin_functor=foo, end_functor=foo)

    blk_list_correct = ['main',
                        'document',
                        'frame',
                        'itemize', 'itemize',
                        'frame',
                        'frame',
                        'frame',
                        'document',
                        'main']

    assert blk_list == blk_list_correct


def test_latex_python_frame():
    latex_code = r'''
\begin{python}{header}
b = 100
\end{python}
\begin{document}
\begin{frame}
\end{frame}

\begin{python}{cpp}
a = 10
\end{python}
\begin{frame}[fragile]
\py{str(a)}
\end{frame}
\end{document}
'''
    tex_struct = LatexStructure()
    tex_struct.parseLatex(latex_code)
    output = str(tex_struct)
    assert latex_code == output

    blk_list = []

    def foo(i):
        blk_list.append(i.name)

    tex_struct.pathInBlock(begin_functor=foo, end_functor=foo)

    blk_list_correct = ['main',
                        'python', 'python',
                        'document',
                        'frame', 'frame',
                        'python', 'python',
                        'frame', 'frame',
                        'document',
                        'main']

    assert blk_list == blk_list_correct


def test_latex_python_special_characters():
    latex_code = r'''
\begin{document}
Toto is \% \\ \& \# \_
Tata
\end{document}
'''
    tex_struct = LatexStructure()
    tex_struct.parseLatex(latex_code)
    output = str(tex_struct)
    assert latex_code == output
