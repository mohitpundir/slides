#!/usr/bin/env python3

import os
import pep8
import Slides
import pytest
# import warnings
# from pylint import epylint as lint


@pytest.fixture
def source_files():
    """
    builds a list of source files. If you find a smarter way, please let me
    know
    """
    print()  # for emacs to evaluate the first line of errors
    mod_files = list()
    for dirpath, _, filenames in os.walk(Slides.__path__[0]):
        mod_files += [os.path.join(dirpath, filename)
                      for filename in filenames
                      if filename.endswith((".py", ".pyx"))]
    return mod_files


def test_pep8_conformity(source_files):
    """
    check all files for pep8 conformity
    """
    pep8style = pep8.StyleGuide()
    pep8style.check_files((mod_f for mod_f in source_files))


# def test_pylint_bitchiness(source_files):
#     print()  # for emacs to evaluate the first line of errors
#     options = ' --rcfile=tests/pylint.rc --disable=locally-disabled '
#     command_options = " ".join(source_files) + options
#     with warnings.catch_warnings():
#         warnings.simplefilter("ignore")
#         lint.py_run(command_options=command_options)
