#!/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
import nbformat
import re
import os


def test_filter_corrections():
    fname = "slides_test.ipynb"
    subprocess.call(f"slides -f {fname} -o {fname}_filtered.ipynb", shell=True)
    notebook = nbformat.read(f"{fname}_filtered.ipynb", as_version=4)
    for cell in notebook.cells:
        if re.search("SolutionToRemove", cell.source):
            print(cell.source)
            raise RuntimeError("Filtering did not work properly")
    if len(notebook.cells) != 3:
        raise RuntimeError("The filtered number of cells should be 3")
    os.remove(f'{fname}_filtered.ipynb')
